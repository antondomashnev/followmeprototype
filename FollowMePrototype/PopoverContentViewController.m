//
//  PopoverContentViewController.m
//  FollowMePrototype
//
//  Created by Anton Domashnev on 17.04.13.
//  Copyright (c) 2013 Anton Domashnev. All rights reserved.
//

#import "PopoverContentViewController.h"

@interface PopoverContentViewController ()

@end

@implementation PopoverContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
