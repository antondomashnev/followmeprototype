//
//  ViewController.h
//  FollowMePrototype
//
//  Created by Anton Domashnev on 17.04.13.
//  Copyright (c) 2013 Anton Domashnev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIWebView *webView;

@end
