//
//  ViewController.m
//  FollowMePrototype
//
//  Created by Anton Domashnev on 17.04.13.
//  Copyright (c) 2013 Anton Domashnev. All rights reserved.
//

#import "ViewController.h"
#import "PopoverContentViewController.h"

@interface ViewController ()<UIPopoverControllerDelegate>

@property (nonatomic, strong) UIPopoverController* popover;
@property (nonatomic, strong) NSString *persistedEnteredURL;

@end

@implementation ViewController
@synthesize persistedEnteredURL = _persistedEnteredURL;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.scrollView.scrollEnabled = NO;
    
    [self addRefreshButton];
    [self addSettingsButton];
    
    NSString *urlString = @"ya.ru";
    if(self.persistedEnteredURL.length>0){
        urlString = self.persistedEnteredURL;
    }
    [self loadWebPageWithURLPath:self.persistedEnteredURL];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Properties

- (void)setPersistedEnteredURL:(NSString *)persistedEnteredURL{
    if(persistedEnteredURL!=_persistedEnteredURL){
        _persistedEnteredURL = persistedEnteredURL;
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        NSString* defaultsKey = @"com.followme.persistedEnteredURL";
        if(!_persistedEnteredURL) {
            [defaults removeObjectForKey:defaultsKey];
        }
        else{
            [defaults setObject:persistedEnteredURL forKey:defaultsKey];
        }
        [defaults synchronize];
    }
}

- (NSString *)persistedEnteredURL{
    if(!_persistedEnteredURL) {
        _persistedEnteredURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"com.followme.persistedEnteredURL"];
    }
    return _persistedEnteredURL;
}

#pragma mark WebView

- (void)loadWebPageWithURLPath:(NSString *)urlPath{
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://%@", urlPath]]]];
}

#pragma mark Add refresh button

- (void)refreshButtonClicked:(id)sender{
    
    [self.webView reload];
}

- (void)addRefreshButton{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshButtonClicked:)];
}

#pragma maro UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    NSString *text = [(PopoverContentViewController *)popoverController.contentViewController urlTextField].text;
    [self loadWebPageWithURLPath:text];
    self.persistedEnteredURL = text;
    
    self.popover = nil;
}

#pragma mark Add settings button

- (void)settingsButtonClicked:(UIBarButtonItem *)sender{

    self.popover = [[UIPopoverController alloc] initWithContentViewController:[[PopoverContentViewController alloc] init]];
    
    [self.popover setPopoverContentSize:CGSizeMake(300, 86)];
    [self.popover setDelegate:self];
    [self.popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)addSettingsButton{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonClicked:)];
}

@end
